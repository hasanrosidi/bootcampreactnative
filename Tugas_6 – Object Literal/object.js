console.log('Tugas 6 – Object Literal')
console.log('\n')
console.log('Soal No. 1 (Array to Object)')
console.log('\n')
/*
	Buatlah function dengan nama arrayToObject() yang menerima sebuah parameter berupa array multidimensi. Dalam array tersebut berisi value berupa First Name, Last Name, Gender, dan Birthyear. Data di dalam array dimensi tersebut ingin kita ubah ke dalam bentuk Object dengan key bernama : firstName, lastName, gender, dan age. Untuk key age ambillah selisih tahun yang ditulis di data dengan tahun sekarang. Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai : “Invalid birth year”.
	
	Contoh: jika input nya adalah [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
	
	maka outputnya di console seperti berikut :
	
	1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
	2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
*/
var now = new Date()
var thisYear = now.getFullYear()
function arrayToObject(arrayData)
{
	if(arrayData.length<=0){
		return console.log("array kosong")
	}
	var newObject = {}
	for(var i=0;i<arrayData.length;i++)
	{
		var tahunLahir = arrayData[i][3];
		var now = new Date()
		var tahunIni = now.getFullYear()
		var age;
		if(tahunLahir && tahunIni-tahunLahir>0){
			age = tahunIni-tahunLahir
		}else
		{
			age = 'Invalid Birth Year'
		}
		
		newObject.firstName = arrayData[i][0]
		newObject.lastName = arrayData[i][1]
		newObject.gender = arrayData[i][2]
		newObject.age = age
		
		var outpurText = (i+1)+'. '+ newObject.firstName +' '+ newObject.lastName
		console.log(outpurText)
		console.log(newObject)
	}
	
}
console.log("=================Soal 1=================")
var contoh = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
arrayToObject(contoh)
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
// Error case 
arrayToObject([]) //-- console.log("array kosong")


console.log('\n')
console.log('Soal No. 2 (Shopping Time)')
/*
	Problem
	
	Diberikan sebuah function shoppingTime(memberId, money) yang menerima dua parameter berupa string dan number. Parameter pertama merupakan memberIddan parameter ke-2 merupakan value uang (money) yang dibawa oleh member tersebut.
	
	Toko X sedang melakukan SALE untuk beberapa barang, yaitu:
	
    Sepatu brand Stacattu seharga 1500000
    Baju brand Zoro seharga 500000
    Baju brand H&N seharga 250000
    Sweater brand Uniklooh seharga 175000
    Casing Handphone seharga 50000
	
	Buatlah function yang akan mengembalikan sebuah object dimana object tersebut berisikan informasi memberId, money, listPurchased dan changeMoney.
	
    Jika memberId kosong maka tampilkan “Mohon maaf, toko X hanya berlaku untuk member saja”
    Jika uang yang dimiliki kurang dari 50000 maka tampilkan “Mohon maaf, uang tidak cukup”
    Member yang berbelanja di toko X akan membeli barang yang paling mahal terlebih dahulu dan akan membeli barang-barang yang sedang SALE masing-masing 1 jika uang yang dimilikinya masih cukup.
	
	Contoh jika inputan memberId: ‘324193hDew2’ dan money: 700000
	
	maka output:
	
	{ memberId: ‘324193hDew2’, money: 700000, listPurchased: [ ‘Baju Zoro’, ‘Sweater Uniklooh’ ], changeMoney: 25000 }
	
	Code
	
	function shoppingTime(memberId, money) {
	// you can only write your code here!
	}
*/

function shoppingTime(memberId, money){
	if (!memberId) {
		return "Mohon maaf, toko X hanya berlaku untuk member saja";
	}else if (money < 50000) 
	{
		return "Mohon maaf, uang tidak cukup";
	} else 
	{
		var dataSale = [ 
			["Sepatu", "Stacattu ", 1500000], 
			["Baju", "Zoro ", 500000], 
			["Baju", "H&N ", 250000], 
			["Sweater", "Uniklooh", 175000], 
			["Casing", "Handphone", 50000]
		];
		var newObject = {}; // memberId, money, listPurchased dan changeMoney
		var moneyChange = money;
		var purchasedList = [];
		//-- sorting dataSale dari yang paling mahal ke murah
		dataSale = dataSale.sort((a, b) => b[2]-a[2]);
		
		
		for(var j=0;j<dataSale.length;j++)
		{
			if(moneyChange>=dataSale[j][2])
			{
				purchasedList.push(dataSale[j][0]+' '+dataSale[j][1])
				moneyChange = moneyChange - dataSale[j][2]
			}
		}
		
		newObject.memberId = memberId
		newObject.money = money
		newObject.listPurchased = purchasedList
		newObject.changeMoney = moneyChange
	}
	return newObject;
}
console.log("=================Soal 2=================")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja


console.log('\n')
console.log('Soal No. 3 (Naik Angkot)')
/*
	Problem
	Diberikan function naikAngkot(listPenumpang) yang akan menerima satu parameter berupa array dua dimensi. Function akan me-return array of object.
	Diberikan sebuah rute, dari A – F. Penumpang diwajibkan membayar Rp2000 setiap melewati satu rute.
	Contoh: input: [ [‘Dimitri’, ‘B’, ‘F’] ] output: [{ penumpang: ‘Dimitri’, naikDari: ‘B’, tujuan: ‘F’, bayar: 8000 }]
	Code
	
	function naikAngkot(arrPenumpang) {
	rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	//your code here
	}
	
	//TEST CASE
	console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
	// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
	//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
	
	console.log(naikAngkot([])); //[]
*/

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	var arrOutput = []
	if (arrPenumpang.length <= 0) {
		return []
	}
	for (var i = 0; i < arrPenumpang.length; i++) 
	{
		var asal = arrPenumpang[i][1];
		var tujuan = arrPenumpang[i][2];
		var indexAsal;
		var indexTujuan;
		for (var j = 0; j < rute.length; j++) 
		{
			if (rute[j] == asal) 
			{
				indexAsal = j
			} else if (rute[j] == tujuan) 
			{
				indexTujuan = j
			}
		}
		var bayar = (indexTujuan - indexAsal) * 2000;
		
		var objOutput = {};
		objOutput.penumpang = arrPenumpang[i][0];
		objOutput.naikDari = asal;
		objOutput.tujuan = tujuan;	
		objOutput.bayar = bayar;
		
		arrOutput.push(objOutput);
	}
	return arrOutput
}
console.log("=================Soal 3=================")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]