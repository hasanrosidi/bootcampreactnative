console.log(`---------------------`)
console.log(`Tugas - 9 -Class`)
console.log(`---------------------`)
console.log(`\n`)
console.log(`---------------------`)
console.log(`1. Animal Class `)
console.log(`---------------------`)
console.log(`a. Release 0`)
class Animal 
{
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
	}
	get name() {
        return this._name
	}
	
    get legs() {
        return this._legs
	}
	
    set legs(leg) {
        this._legs = leg
	}
	
    get cold_blooded() {
        return this._cold_blooded
	}
	
    set cold_blooded(blood) {
        this._cold_blooded = blood
	}
}	
var sheep = new Animal("shaun");

console.log(sheep.name) //-- shaun
console.log(sheep.legs) //--4
console.log(sheep.cold_blooded) //--false

console.log(`\n`)
console.log(`b. Release 1`)
class Ape extends Animal {
    constructor(name) {
        super(name)
        super.legs = 2
	}
	
    yell() {
        return console.log('Auooo')
	}
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        super.cold_blooded = true
	}
	
    jump() {
        return console.log('hop hop')
	}
}

console.log(`\n`)
const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"

console.log(`\n`)
const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"

console.log(`\n`)
console.log(`---------------------`)
console.log(`2. Function to Class`)
console.log(`---------------------`)

class Clock {
	constructor({ template }) {
		this._timer = null;
        this._template = template;

	}
	
	render() {
		var _date = new Date();
		
		var hours = _date.getHours();
		if (hours < 10) hours = '0' + hours;
		
		var mins = _date.getMinutes();
		if (mins < 10) mins = '0' + mins;
		
		var secs = _date.getSeconds();
		if (secs < 10) secs = '0' + secs;
		
		var output = this._template
		.replace('h', hours)
		.replace('m', mins)
		.replace('s', secs);
		
		console.log(output);
	}
	
	stop() {
		clearInterval(this.timer);
		
	}
	
	start() {
		this.render();
        this._timer = setInterval(() => this.render(), 1000);
		//console.log(this._timer)
	}
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();
//clock.stop();

