import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button ,SafeAreaView,FlatList, Touchable} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Data }from './data'
export default function Home({route,navigation}) {
    const {username,password} = route.params;

    const [totalPrice, setTotalPrice] = useState(0);

     const currencyFormat=(num)=> {
         return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
       };

    const updateHarga =(price)=>{
         console.log("UpdatPrice : " + price);
         const temp = Number(price) + totalPrice;
         console.log(temp)
         setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js -- 
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }

    
 
    return (
        
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>Hai {username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 20}}>
            {
            /* //? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->

            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */
             <FlatList
                columnWrapperStyle={{justifyContent:'space-between', }}
                horizontal={false}
                numColumns={2}
                data={Object.keys(Data)}
                renderItem={({ item }) => 
                (
                    <View style={styles.content}>
                        <Text style={{flex:1, }}>{Data[item].title}</Text>
                        <Image style={{height:100,width:100,borderWidth:0,alignSelf:'center'}}  source={Data[item].image}/>
                        <Text style={{flex:1, }}>{Data[item].harga}</Text>
                        <Text style={{flex:1, }}>{Data[item].desc}</Text>
                        <Text style={{flex:1, }}>{Data[item].type}</Text>
                        <Button
                            title="Beli"
                            onPress={()=>updateHarga(Data[item].harga)}
                        />
                    </View>
                )}
                keyExtractor={(item) => Data.id}
                />
             }
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop:5,
        backgroundColor: '#ecf0f1',
        padding: 8,
      },
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        padding:5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
        
})
