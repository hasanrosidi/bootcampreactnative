import React from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default function Form({
	title,
	secureTextEntry,
	value,
	setValue = () => {},
}) {
	return (
		<View style={styles.formContainer}>
			<Text style={styles.formTitle}>{title}</Text>
			<View style={styles.inputContainer}>
				<TextInput
					style={styles.formInput}
					secureTextEntry={secureTextEntry}
					value={value}
					onChangeText={(text) => setValue(text)}
				/>
				{secureTextEntry && (
					<MaterialCommunityIcons name="eye" size={24} color="black" />
				)}
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	formContainer: {
		marginBottom: 20,
		width: "100%",
	},
	formTitle: {
		fontSize: 14,
		fontWeight: "bold",
		marginBottom: 7,
	},
	formInput: {
		flex: 1,
	},
	inputContainer: {
		flexDirection: "row",
		borderWidth: 1,
		borderColor: "grey",
		borderRadius: 8,
		padding: 10,
	},
});
