import React from 'react'
import { TextInput } from 'react-native-gesture-handler'
import { Button, StyleSheet, Text, View } from 'react-native'

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Text>Login Screen</Text>
            <TextInput 
                placeholder="username"
            />
            <TextInput 
                placeholder="password"
            />
            <Button onPress={()=>navigation.navigate("MyDrawwer",{
                screen: 'App',param:{
                    screen:'AboutScreen'
                }
            })} title="Menuju Halaman Home"/>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    }
})
