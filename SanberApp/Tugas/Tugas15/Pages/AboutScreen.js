import React from 'react';
import {View,Text, Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, ImageBackground, TextInput } from 'react-native';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';
import { AntDesign as IconAntDesign} from '@expo/vector-icons';
import { FontAwesome as IconFontAwesome} from '@expo/vector-icons';

export default function LoginScreen(){
    return(
        <View style={styles.container}>
            <View style={styles.top}>
                <Image style={{height:126,width:353,borderWidth:0,alignSelf:'center'}}   source={require('./images/Logo.png')} />
            </View>
            <View style={styles.middle}>
                <Text style={styles.aboutUsTitle}>Tentang Saya</Text>
                <Image style={{height:100,width:100,alignSelf:'center',borderWidth:1,borderRadius:10,padding:5}}   source={require('./images/ava_3.png')} />
                <Text style={styles.aboutUsName}>Hasan Rosidi</Text>
                <Text style={styles.aboutUsRole}>React Native Developer</Text>
                <View style={styles.aboutUsDetailWrapper}>
                    <View style={styles.aboutUsPortofolio}>
                        <Text style={{borderBottomColor: 'black',borderBottomWidth: 1,fontWeight:'bold'}}>Portofolio</Text>   
                        <View style={{flexDirection:'row'}}>
                            <View style={styles.aboutUsPortofolioList}>
                                <Icon name="github" size={32} color="black" />
                                <Text style={{fontWeight:'bold'}}>@hasanrosidi</Text>
                            </View>
                            <View style={styles.aboutUsPortofolioList}>
                                <Icon name="gitlab" size={32} color="black" />
                                <Text style={{fontWeight:'bold'}}>@hasanrosidi</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.aboutUsPortoContact}>
                        <Text style={{borderBottomColor: 'black',borderBottomWidth: 1,fontWeight:'bold'}}>Contact</Text>
                        <View style={{flexDirection:'column'}}>
                            <View style={styles.aboutUsContactList}>
                                <Image style={{height:24,width:24,alignSelf:'center'}}   source={require('./images/logo-facebook.png')} />
                                <Text style={{fontWeight:'normal',color:'#003366'}}>hasanrosidi</Text>
                            </View>
                            <View style={styles.aboutUsContactList}>
                                <Image style={{height:24,width:24,alignSelf:'center'}}   source={require('./images/logo-instagram.png')} />
                                <Text style={{fontWeight:'normal',color:'#003366'}}>hasanrosidi@gmail.com</Text>
                            </View>
                            <View style={styles.aboutUsContactList}>
                                <Image style={{height:24,width:24,alignSelf:'center'}}   source={require('./images/logo-twitter.png')} />
                                <Text style={{fontWeight:'normal',color:'#003366'}}>hasanrosidi</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    aboutUsTitle  :{
        fontWeight:'normal',
        color:'black',
        fontSize:20,
        marginBottom:10,
        alignSelf:'center'
    },
    aboutUsName  :{
        fontWeight:'normal',
        color:'black',
        fontSize:14,
        marginTop:10,
        marginBottom:10,
        alignSelf:'center'
    },
    aboutUsRole  :{
        fontWeight:'normal',
        color:'black',
        fontSize:14,
        marginBottom:10,
        alignSelf:'center'
    },
    aboutUsDetailWrapper:{
        flex:1,
        fontSize:12,
        padding:30,
    },
    aboutUsPortofolio:{
        flex:1,
        fontSize:12,
        backgroundColor:'#C8C8C8',
        textAlignVertical: "top", 
        marginTop:5,  
        marginBottom:5, 
        padding:5,  
    },
    aboutUsPortofolioList:{
        marginLeft:5,
        marginRight:5,
    },
    aboutUsContactList:{
        flexDirection:'row',
        marginLeft:5,
        marginRight:5,
    },
    aboutUsPortoContact:{
        fontSize:12,
        backgroundColor:'#C8C8C8',
        textAlignVertical: "top",
        marginTop:5,  
        marginBottom:5,  
        padding:5, 
    },
    container: {
        flex: 1,
        padding: 2,
        backgroundColor: "#ffffff",
      },
      top: {
        flex: 1,
        alignContent:"center"
      },
      middle: {
        flex: 2,
        alignContent:'center'
      },
    

})