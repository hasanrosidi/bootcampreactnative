import React from 'react';
import {View,Text, Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, ImageBackground, TextInput } from 'react-native';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';

export default function AboutScreen(){
    return(
        <View style={styles.container}>
        <View style={styles.top}>
            <Image style={{height:126,width:353,borderWidth:0,alignSelf:'center'}}   source={require('./images/Logo.png')} />
        </View>
        <View style={styles.middle}>
            <Text style={styles.formContainerTitle}>Login</Text>
            <View style={styles.formContainer}>
                <Text style={styles.formTitle}>Username</Text>
                <View style={styles.formInput}>
                    <TextInput
                        style={styles.formInputField}
                        placeholder="username"
                    />
                </View>
                <Text style={styles.formTitle}>Password</Text>
                <View style={styles.formInput}>
                    <TextInput
                        style={styles.formInputField}
                        placeholder="password"
                    />
                    <Icon name="eye-circle-outline" size={24} color="grey" />
                </View>    
            </View>
        </View>
        <View style={styles.bottom}>
            <TouchableOpacity style={styles.buttonRegister}>
                <Text style={styles.textRegister}>Daftar</Text>
            </TouchableOpacity>
            <Text >Atau</Text>
            <TouchableOpacity style={styles.buttonLogin}>
                <Text style={styles.textLogin}>Masuk</Text>
            </TouchableOpacity>
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    formContainerTitle  :{
        fontWeight:'bold',
        color:'#003366',
        fontSize:20,
        alignSelf:'center'
    },
    formContainer:{
        padding: 20
    },
    formTitle:{
        marginTop: 10,
        marginBottom: 5,
        fontWeight:'normal',
        fontSize:16
    },
    formInput: {
        flexDirection:'row',
        borderWidth:1,
        borderColor:'grey',
        borderRadius:3,
        padding:6
    },
    formInputField: {
        flex:1,
        fontSize:16 
      },
    container: {
        flex: 1,
        padding: 2,
        backgroundColor: "#ffffff",
      },
      top: {
        flex: 1,
        alignContent:"center"
      },
      middle: {
        flex: 1,
        alignContent:'center'
      },
      bottom: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center', 
        backgroundColor: "#ffffff",
      },
      buttonRegister:{
        width: 150,
		height: 40,
		backgroundColor: "#003366",
		justifyContent: "center",
		alignItems: "center",       
        borderRadius:5
      },
      textRegister:{
        color:'#ffffff',
        fontWeight:'bold'
      },
      buttonLogin:{
        width: 150,
        height: 40,
        backgroundColor: "#ffffff",
        justifyContent: "center",
        alignItems: "center",   
        color:'#3EC6FF',
        borderColor:'#003366',
        borderWidth:1,
        borderRadius:3
      },
      textLogin:{
        color:'#3EC6FF',
        fontWeight:'bold'
      },
})