import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

export default function Button({ title, type = "filled" }) {
	return (
		<TouchableOpacity style={styles.buttonContainer(type === "filled")}>
			<Text style={styles.buttonTitle(type === "filled")}>{title}</Text>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	buttonContainer: (isFilled) => ({
		padding: 10,
		borderRadius: 8,
		backgroundColor: isFilled ? "blue" : "white",
		alignItems: "center",
		justifyContent: "center",
		borderWidth: isFilled ? 0 : 1,
		borderColor: "blue",
	}),
	buttonTitle: (isFilled) => ({
		fontWeight: "bold",
		color: isFilled ? "white" : "blue",
		textTransform: "uppercase",
	}),
});
