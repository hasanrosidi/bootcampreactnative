import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const Card = ({ title, years }) => {
	return (
		<TouchableOpacity style={styles.Button2}>
			<Text>{title}</Text>
			<Text>{years}</Text>
		</TouchableOpacity>
	);
};

export default function Props() {
	return (
		<View style={styles.container}>
			<Card title="monkey" years="2012" />
			<Card title="One Piece" years="1998" />
			<Card title="Naruto" years="2010" />
			<Card title="Saitama" years="2016" />
			<Card title="Dragon ball" years="2010" />
			<Card title="Boku No Hero" years="2010" />
		</View>
	);
}

const styles = StyleSheet.create({
	Button2: {
		width: 300,
		height: 30,
		backgroundColor: "skyblue",
		justifyContent: "center",
		alignItems: "center",
	},
	container: {
		padding: 16,
	},
});
