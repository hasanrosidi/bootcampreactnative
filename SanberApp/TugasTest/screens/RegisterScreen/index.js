import React from "react";
import {
	FlatList,
	Image,
	SafeAreaView,
	ScrollView,
	StyleSheet,
	Text,
	TextInput,
	View,
} from "react-native";

import Button from "../../components/Button";
import Form from "../../components/Form";

export default function RegisterScreen() {
	const FORM_DATA = [
		{ id: "1", title: "Username", isSecureTextEnty: false },
		{ id: "11", title: "Nama", isSecureTextEnty: false },
		{ id: "12", title: "Alamat", isSecureTextEnty: false },
		{ id: "13", title: "Profesi", isSecureTextEnty: false },
		{ id: "2", title: "Email", isSecureTextEnty: false },
		{ id: "3", title: "Password", isSecureTextEnty: true },
		{ id: "4", title: "Ulangi Password", isSecureTextEnty: true },
	];
	return (
		<SafeAreaView style={styles.container}>
			<FlatList
				data={FORM_DATA}
				renderItem={({ item }) => (
					<Form title={item.title} secureTextEntry={item.isSecureTextEnty} />
				)}
				ListHeaderComponent={() => (
					<>
						<Image
							source={require("../../assets/images/Logo.png")}
							style={styles.logo}
						/>
						<Text style={styles.title}>Daftar</Text>
					</>
				)}
				ListFooterComponent={() => (
					<View style={styles.footer}>
						<Button title="Daftar" type="filled" />
						<Text style={styles.footerText}>atau</Text>
						<Button title="Masuk" type="outlined" />
					</View>
				)}
			/>

			{/* <View style={styles.form}> */}
			{/* {FORM_DATA.map((value) => (
						<Form
							key={value.id}
							title={value.title}
							secureTextEntry={value.isSecureTextEnty}
						/>
					))} */}
			{/* <Form title="Username" />
					<Form title="Email" />
					<Form title="Password" secureTextEntry />
					<Form title="Ulangi Password" secureTextEntry /> */}
			{/* </View> */}
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 20,
		paddingHorizontal: 20,
	},
	logo: {
		marginVertical: 20,
		alignSelf: "center",
	},
	title: {
		fontSize: 24,
		fontWeight: "bold",
		marginBottom: 20,
	},
	form: {
		flex: 1,
		marginVertical: 20,
		alignItems: "flex-start",
	},

	footer: {
		marginVertical: 20,
	},
	footerText: {
		alignSelf: "center",
		marginVertical: 5,
	},
});
