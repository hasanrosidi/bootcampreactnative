import React from "react";
import {
	Image,
	SafeAreaView,
	ScrollView,
	StyleSheet,
	Text,
	TextInput,
	View,
} from "react-native";

import Button from "../../components/Button";
import Form from "../../components/Form";

export default function LoginScreen() {
	return (
		<SafeAreaView style={styles.container}>
			<ScrollView>
				<Image
					source={require("../../assets/images/Logo.png")}
					style={styles.logo}
				/>
				<Text style={styles.title}>Login</Text>
				<View style={styles.form}>
					<Form title="Username/Email" />
					<Form title="Password" secureTextEntry />
				</View>
			</ScrollView>
			<View style={styles.footer}>
				<Button title="Masuk" type="filled" />
				<Text style={styles.footerText}>atau</Text>
				<Button title="Daftar" type="outlined" />
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 20,
		paddingHorizontal: 20,
	},
	logo: {
		marginVertical: 20,
		alignSelf: "center",
	},
	title: {
		fontSize: 24,
		fontWeight: "bold",
		marginBottom: 20,
	},
	form: {
		flex: 1,
		marginVertical: 20,
		alignItems: "flex-start",
	},

	footer: {
		marginVertical: 20,
	},
	footerText: {
		alignSelf: "center",
		marginVertical: 5,
	},
});
