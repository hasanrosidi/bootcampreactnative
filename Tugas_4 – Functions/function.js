console.log('Tugas 4 – Functions');
console.log('\n');
console.log('No. 1');
function teriak() 
{
	return "Halo Sanbers!"
}
console.log(teriak()) // "Halo Sanbers!" 
console.log('\n');

// Soal 2
console.log('No. 2');
function kalikan(x, y) 
{
  return x * y
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);
console.log('\n');

// Soal 3
console.log('No. 3');
function introduce(name, age, address, hobby) 
{
  var kalimat = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
  return kalimat
} 
var name = "Hasan Rosidi";
var age = 30;
var address = "Jln. Mayjend Sungkono VI, Amany Regency";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); 