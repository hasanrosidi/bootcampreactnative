import React, { useState, useEffect } from 'react';
import {View,Text, Image, StyleSheet, Button,SafeAreaView, FlatList, TouchableOpacity, ImageBackground, TextInput } from 'react-native';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';
import axios from 'axios';
//import AsyncStorage from '@react-native-community/async-storage';

import HomeScreen from '../HomeScreen';

export default function LoginScreen({ navigation }){
    const [username, setUsername]=useState("");
    const [password, setPassword]=useState("");
    const [notifUserPass, setNotifUserPass]=useState("");
    const submit =()=>{
        const body ={
            username:username, password:password
        }
        axios.post('http://192.168.0.6/myapi/api/login', body)
        .then(res=>{
            //console.log('res: ', res)
            setUsername("")
            setPassword("")
            //const data = JSON.parse(res)
            //console.log(res.data)
            //console.log(res.data['err'])
            if(res.data['err']==0)
            {
                //-- login berhasil
                //-- navigate to router.js / home page
                //return navigation.navigate('router', {username});
                setNotifUserPass("user dan password anda benar");
                return navigation.navigate('HomeScreen', {username});
            }else{
                //-- login salah
                //alert('login salah')
                setNotifUserPass("user dan password anda salah");
            }
            
        }).catch(err=>{
            //console.log('error: ', err)
        })
    }

    /*
    const saveToken = async (item) => {
        try {
            if(item !== null) {
                await AsyncStorage.setItem("token", item.data.token)
            }
        } catch (err) {
            console.log(err)
        }
    }
    */
   
    return(
        
        <View style={styles.container}>
        <View style={styles.top}>
            <ImageBackground source={require('../images/large.png')} resizeMode="contain" style={{flex: 1, justifyContent: "center"}}>
            </ImageBackground>
        </View>
        <View style={styles.middle}>
            <View style={styles.formContainer}>
                <Text style={styles.formTitle}>Username</Text>
                <View style={styles.formInput}>
                    <TextInput
                        style={styles.formInputField}
                        placeholder="username"
                        value={username}
                        onChangeText={(value)=>setUsername(value)}
                    />
                </View>
                <Text style={styles.formTitle}>Password</Text>
                <View style={styles.formInput}>
                    <TextInput
                        textContentType="password"
                        style={styles.formInputField}
                        placeholder="password"
                        value={password}
                        onChangeText={(value)=>setPassword(value)}
                    />
                    <Icon name="eye-circle-outline" size={24} color="grey" />
                </View> 

                <Text>{notifUserPass}</Text> 
                <TouchableOpacity style={styles.buttonRegister} onPress={submit}>
                    <Text style={styles.textLogin}>Login</Text>
                </TouchableOpacity>
            </View>
        </View>
        <View style={styles.bottom}>
            
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    formContainerTitle  :{
        fontWeight:'bold',
        color:'#003366',
        fontSize:20,
        alignSelf:'center'
    },
    formContainer:{
        padding: 20
    },
    formTitle:{
        marginTop: 10,
        marginBottom: 5,
        fontWeight:'normal',
        fontSize:16
    },
    formInput: {
        flexDirection:'row',
        borderWidth:1,
        borderColor:'grey',
        borderRadius:3,
        padding:6
    },
    formInputField: {
        flex:1,
        fontSize:16 
      },
    container: {
        flex: 1,
        padding: 2,
        backgroundColor: "#ffffff",
    },
    top: {
        flex: 1,
        alignContent:"center",
    },
    middle: {
        flex: 2,
        alignContent:'center',
        justifyContent: 'center', 
    },
    bottom: {
        flex: 0.5,
        flexDirection:'row',
        justifyContent: 'center', 
        backgroundColor: "#ffffff",
    },
      buttonRegister:{
        width: 150,
		height: 40,
		backgroundColor: "#003366",
		justifyContent: "center",
		alignItems: "center", 
        alignSelf: "center",    
        borderRadius:5
      },
      textRegister:{
        color:'#ffffff',
        fontWeight:'bold'
      },
      buttonLogin:{
        width: 150,
        height: 40,
        backgroundColor: "#ffffff",
        justifyContent: "center",
        alignItems: "center",   
        color:'#3EC6FF',
        borderColor:'#003366',
        borderWidth:1,
        borderRadius:3
      },
      textLogin:{
        color:'#3EC6FF',
        fontWeight:'bold'
      },
})
