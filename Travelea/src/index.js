import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import AboutxScreen from './AboutScreen';

const Stack = createStackNavigator();

export default function index() {
    return (
       <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='AboutScreen' component={AboutxScreen} options={{ headerTitle: 'About You' }} />
          <Stack.Screen name='HomeScreen' component={HomeScreen} options={{ headerTitle: 'Travel wish' }} />
        </Stack.Navigator>
      </NavigationContainer>
    )
}
