import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image,FlatList, Touchable} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from 'axios';

export default function HomeScreen({route,navigation}) {
    const {username} = route.params;
    const [travelWish, setTravelWish] = useState({})

    useEffect(() => {
      getTravelWish()
  }, [])

    const getTravelWish = () => {
      axios.get('http://192.168.0.6/myapi/api/getTravelWish', {
            headers: {
                'Authorization': 'Bearer',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then((res) => {
            console.log(res.data)
            setTravelWish(res.data)
        })
        .catch((err) => {
            console.log("err", err)  
        })
    }
    const goToAbout =()=>{
      return navigation.navigate('AboutScreen', {username});
    }

    return (
      <View style={styles.container}>
        <View style={{  padding: 2}}>
            <TouchableOpacity style={{flexDirection:'column',justifyContent:"space-between",height:30}} onPress={goToAbout}>
              <Text style={{flex:1,fontSize:18, fontWeight:'bold'}}>Hai Selamat Datang , {username}</Text>
          </TouchableOpacity>
        </View>

        <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 20}}>
            {
             <FlatList
                columnWrapperStyle={{justifyContent:'space-between', }}
                horizontal={false}
                numColumns={2}
                data={Object.keys(travelWish)}
                renderItem={({item}) => 
                (
                    <View style={styles.content}>
                        
                        <Image
                          source={{uri: travelWish[item].image_small}}
                          style={{width:160, height: 140, resizeMode : 'stretch', borderRadius:4}}
                      />
                        <View style={{
                          position: 'absolute', top: 0, left: 20, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', width:100}}>
                          <Text style={{color:'#fefefe'}}>{travelWish[item].location}</Text>
                      </View>  

                      <TouchableOpacity >
                        <Text style={{
                          fontSize:14, fontWeight:'bold', margin:10,borderWidth:1, borderRadius:3,
                          paddingTop:2,paddingBottom:2, paddingLeft:10, paddingRight:10,
                          marginBottom:10, width:160, textAlign:'center', backgroundColor:'#003366', color:'#ffffff'
                      }}>{travelWish[item].name}</Text>
                      </TouchableOpacity>
                        
                    </View>
                )}
                keyExtractor={(item) => travelWish.id}
                />
             }
            </View>

      </View>
    );
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'flex-start',
      marginTop:10,
      backgroundColor: '#ecf0f1',
      padding: 5,
    },
  content:{       
      margin: 0,
      padding:0,
      width:180,
      height:180,
      alignItems:'center',
      borderColor:'grey',   
  },
  image: {
    height:100,width:100,
    justifyContent: "center"
  }, 
})