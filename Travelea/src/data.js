const Data =[
    {
        id : "1",
        name: "Rome",
        image_small: require("./images/rome.jpg"),
        image_big : require("./images/rome_big.jpg"),
        location : "Roma Italia",
        price :1000
    },
    {
        id : "2",
        name: "Paris",
        image_small: require("./images/paris.jpg"),
        image_big : require("./images/paris_big.jpg"),
        location : "Paris France",
        price :1200
    },
    {
        id : "3",
        name: "Swis Alphin",
        image_small: require("./images/swiss_alphin.jpg"),
        image_big : require("./images/swiss_alphin_big.jpg"),
        location : "The Swiss Alps comprise almost all the highest mountains of the AlpsSwitzerland",
        price :1100
    },
    {
        id : "4",
        name: "Bali Klungkung",
        image_small: require("./images/bali_klungkung.jpg"),
        image_big : require("./images/bali_klungkung_big.jpg"),
        location : "Nusa Penida, Klungkung Regency, Bali, Indonesia",
        price :1050
    },
]
export {Data};