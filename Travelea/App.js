import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Src from './src';

export default function App() {
  return (
    <Src/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
