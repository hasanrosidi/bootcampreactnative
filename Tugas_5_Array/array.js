console.log('Tugas 5 – Array');
console.log('\n');
/*
	Soal No. 1 (Range)
	Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).
*/
function range(startNumber, endNumber) {
	var rangeArr = [];
	if (startNumber > endNumber) {
		var rangeLength = startNumber - endNumber + 1;
		for (var i = 0; i < rangeLength; i++) {
			rangeArr.push(startNumber - i)
		}
		} else if (startNumber < endNumber) {
		var rangeLength = endNumber - startNumber + 1;
		for (var i = 0; i < rangeLength; i++) {
			rangeArr.push(startNumber + i)
		}
		} else if (!startNumber || !endNumber) {
		return -1
	}
	return rangeArr
}

console.log("Soal No. 1 (Range)")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


/*
	Soal No. 2 (Range with Step)
	
	Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.
*/
function rangeWithStep(startNumber, endNumber, step) {
	var rangeArr = [];
	if (startNumber > endNumber) {
		var currentNum = startNumber;
		for (var i = 0; currentNum >= endNumber; i++) {
			rangeArr.push(currentNum)
			currentNum -= step
		}
		} else if (startNumber < endNumber) {
		var currentNum = startNumber;
		for (var i = 0; currentNum <= endNumber; i++) { 
			rangeArr.push(currentNum) 
			currentNum += step
		}
		} else if (!startNumber || !endNumber || !step) {
		return -1
	}
	return rangeArr
}
console.log('\n');
console.log("Soal No. 2 (Range with Step)");
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

/*
	Soal No. 3 (Sum of Range)
	
	Kali ini kamu akan menjumlahkan sebuah range (Deret) yang diperoleh dari function range di soal-soal sebelumnya. Kamu boleh menggunakan function range dan rangeWithStep pada soal sebelumnya untuk menjalankan soal ini.
	
	Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka. contohnya sum(1,10,1) akan menghasilkan nilai 55.
	
	ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1.
*/

function sum(startNumber, endNumber, step) {
	var rangeArr = [];
	var distance;
	
	if (!step) {
		distance = 1
	} else 
	{
		distance = step
	}
	
	if (startNumber > endNumber) 
	{
		var currentNum = startNumber;
		for (var i = 0; currentNum >= endNumber; i++) 
		{
			rangeArr.push(currentNum)
			currentNum -= distance
		}
	} else if (startNumber < endNumber) 
	{
		var currentNum = startNumber;
		for (var i = 0; currentNum <= endNumber; i++) 
		{
			rangeArr.push(currentNum)
			currentNum += distance
		}
	} else if (!startNumber && !endNumber && !step) 
	{
		return 0
	} else if (startNumber) 
	{
		return startNumber
	}
	
	var total = 0;
	for (var i = 0; i < rangeArr.length; i++) {
		total = total + rangeArr[i]
	}
	return total
}


console.log('\n');
console.log("Soal No. 3 (Sum of Range)");
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

/*
	Soal No. 4 (Array Multidimensi)
	
	Sering kali data yang diterima dari database adalah array yang multidimensi (array di dalam array). Sebagai developer, tugas kita adalah mengolah data tersebut agar dapat menampilkan informasi yang diinginkan.
	
	Buatlah sebuah fungsi dengan nama dataHandling dengan sebuah parameter untuk menerima argumen. Argumen yang akan diterima adalah sebuah array yang berisi beberapa array sejumlah n. Contoh input dapat dilihat dibawah:
	
	//contoh input
	var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
	] 
	
	Tugas kamu adalah mengimplementasikan fungsi dataHandling() agar dapat menampilkan data-data pada dari argumen seperti di bawah ini:
*/
var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], //--0
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], //--1
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], //--2
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"], //--3
	["0005", "Hasan", "Malang", "6/4/1990", "Gaming"] //--4
];

function dataHandling(data){
	var dataLength = data.length;
	for(var i=0; i<dataLength; i++)
	{
		console.log("Nomor ID	: " + data[i][0]);
		console.log("Nama Lengkap	: " + data[i][1]);
		console.log("TTL		: " + data[i][2]);
		console.log("Hobi		: " + data[i][3]);
		console.log();
	}
	
}

console.log('\n');
console.log("Soal No. 4 (Array Multidimensi)");
console.log(dataHandling(input));

/*
	Soal No. 5 (Balik Kata)
	
	Kamu telah mempelajari beberapa method yang dimiliki oleh String dan Array. String sebetulnya adalah sebuah array karena kita dapat mengakses karakter karakter pada sebuah string layaknya mengakses elemen pada array.
	
	Buatlah sebuah function balikKata() yang menerima sebuah parameter berupa string dan mengembalikan kebalikan dari string tersebut.
*/
console.log('\n');
console.log("Soal No. 5 (Balik Kata)");
function balikKata(data){
	var newKata="";
	for(var i=data.length-1; i>=0; i--)
	{
		newKata += data[i];
	}
	return newKata;
}
function balikKataDoWhile(data)
{
	var newKata="";
	var i=data.length-1;
	do{
		newKata += data[i];
		i=i-1;
	}
	while(i>=0)
	return newKata;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar")); 
console.log(balikKata("I am Sanbers"));  
console.log(balikKata("abcdeffghijklmnopqrstuvwxyz")); 
console.log(balikKataDoWhile("Balik Kata Do While")); 



/*
	Soal No. 6 (Metode Array)
	
	Array pada JavaScript memiliki sekumpulan built-in function yang digunakan untuk mempermudah developer untuk mengolah data di dalamnya. Beberapa fungsi yang sering digunakan antara lain join, split, slice, splice, dan sort. Kerjakanlah tantangan ini untuk memperkuat pengertian kamu tentang built-in function tersebut.
	
	Tujuan

Mengerti Kegunaan dari Built-in Function yang dimiliki Array
Mampu Menggunakan Built-in Function yang dimiliki Array
Petunjuk
//contoh input
["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
*/
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];


function dataHandling2(data)
{
	var newData = data;
	var newName = data[1] + 'Elsharawy';
	var newProvince = 'Provinsi ' + data[2];
	var gender = 'Pria';
	var sekolah = 'SMA Internasional Metro';
	
	newData.splice(1,1,newName);
	newData.splice(2,1,newProvince);
	newData.splice(4,1,gender);
	newData.splice(5,1,sekolah);
	
	var bulan = data[3];
	var newDate = bulan.split('/'); //-- [21,05,1989]
	var monthNumber = newDate[1]
	var monthName = "";
	switch(monthNumber) {
		case '01': monthName = "Januari"; break;
		case '02': monthName = "Februari"; break;
		case '03': monthName = "Maret"; break;
		case '04': monthName = "April"; break;
		case '05': monthName = "Mei"; break;
		case '06': monthName = "Juni"; break;
		case '07': monthName = "Juli"; break;
		case '08': monthName = "Agustus"; break;
		case '09': monthName = "September"; break;
		case '10': monthName = "Oktober"; break;
		case '11': monthName = "November"; break;
		case '12': monthName = "Desember"; break;
		default:
		break;
	}
	
	var dateStrip = newDate.join('-')
	var dateSortDesc = newDate.sort(function (value1, value2) { return value2 - value1 }) 
	var firtNameMiddleName = newName.slice(0, 15)
	
	console.log(newData)
	console.log(monthName) 
	console.log(dateSortDesc)
	console.log(dateStrip) 
	console.log(firtNameMiddleName)
}
/**
	* keluaran yang diharapkan (pada console)
	*
	* ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
	* Mei
	* ["1989", "21", "05"]
	* 21-05-1989
	* Roman Alamsyah
*/
console.log('\n');
console.log("Soal No. 6 (Metode Array)");
console.log(dataHandling2(input));