var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var listBuku = books.length;
var time = 10000;

function membaca(time, idx, listBuku) {
    readBooksPromise(time, books[idx])
        .then(function (sisaWaktu) {
            listBuku = listBuku - 1;
            if (listBuku > 0 && sisaWaktu >= books[idx].timeSpent) {
                membaca(sisaWaktu, idx+1, listBuku);
            }
        })
        .catch(function (error) {
            //console.log(error)
        })
}

membaca(time, 0, listBuku);
