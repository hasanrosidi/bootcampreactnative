/*
	Tugas 7 - Es6
*/
console.log(`1. Mengubah fungsi menjadi fungsi arrow`)
/*
 const golden = function goldenFunction(){
  console.log("this is golden!!")
}
*/
const golden = () => {
    console.log("this is golden!!")
}
golden()
console.log(`\n`)
console.log(`2. Sederhanakan menjadi Object literal di ES6`)
/*
 const newFunction = function literal(firstName, lastName){
   return {
     firstName: firstName,
     lastName: lastName,
     fullName: function(){
       console.log(firstName + " " + lastName)
       return 
     }
   }
 }
*/
const newFunction = function literal(firstName, lastName) {
	return {
		firstName,
		lastName,
		fullName: function () {
			console.log(firstName + " " + lastName);
			return;
		},
	};
};
//Driver Code
const person = newFunction("William", "Imoh");
console.log(person);
person.fullName();

console.log(`\n`)
console.log(`3. Destructuring`)
/*
Diberikan sebuah objek sebagai berikut:
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
*/
// -- dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
//const firstName = newObject.firstName;
//const lastName = newObject.lastName;
//const destination = newObject.destination;
//const occupation = newObject.occupation;

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
// ES6
const { firstName, lastName, destination, occupation } = newObject
// Driver code
console.log(firstName, lastName, destination, occupation)

console.log(`\n`)
console.log(`4. Array Spreading`)
console.log(`Kombinasikan dua array berikut menggunakan array spreading ES6`)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log(`\n`)
console.log(`5. Template Literals`)
/*
	sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + ' dolor sit amet, ' +  
    'consectetur adipiscing elit, ' + planet + ' do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
*/	
const planet = "earth"
const view = "glass"

const before = `Lorem  ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
// Driver Code
console.log(before) 