console.log('\n');
console.log('B. Tugas Conditional');
console.log('1. If-else');
//--penyihir, guard, dan werewolf.
var nama = "Hasan"
var peran = "werewolf"
if ( nama=="" ) {
    console.log("Nama harus diisi!");
}
if ( peran=="" ) {
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game");
}

if ( nama!="" && peran=="penyihir") {
	console.log("Selamat datang di Dunia Werewolf, "+nama);
	console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}

if ( nama!="" && peran=="guard") {
	console.log("Selamat datang di Dunia Werewolf, "+nama);
	console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}

if ( nama!="" && peran=="werewolf") {
	console.log("Selamat datang di Dunia Werewolf, "+nama);
	console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!");
}

console.log('\n');
console.log('2. Switch Case');
var hari = 12; 
var bulan = 1; 
var tahun = 2000;
switch(bulan) {
 case 0: bulan = "Januari"; break;
 case 1: bulan = "Februari"; break;
 case 2: bulan = "Maret"; break;
 case 3: bulan = "April"; break;
 case 4: bulan = "Mei"; break;
 case 5: bulan = "Juni"; break;
 case 6: bulan = "Juli"; break;
 case 7: bulan = "Agustus"; break;
 case 8: bulan = "September"; break;
 case 9: bulan = "Oktober"; break;
 case 10: bulan = "November"; break;
 case 11: bulan = "Desember"; break;
}
console.log("Maka hasil yang akan tampil di console adalah:" + hari +" "+ bulan +" "+ tahun);